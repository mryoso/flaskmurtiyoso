FROM python:3.6.5-alpine

#Set Wokdir
WORKDIR /code

#install requirements
COPY ./requirements.txt /code
run pip install -r requirements.txt

COPY . /code

CMD ["python","manage.py"]